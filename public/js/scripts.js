//Cada vez que alguien copie algún bloque de texto de la página, automáticamente le agregamos nuestro crédito .
document.body.oncopy = function() {
    alert('Todos los derechos reservados. Amatop © 2021');
    return false;
};

//TEXTO PARPADEANTE
window.setInterval(BlinkIt, 500);
var color = "#566787";

function BlinkIt() {
    var blink = document.getElementById("blink");
    color = (color == "black") ? "#566787" : "black";
    blink.style.color = color;
}

// reloj

function mueveReloj() {
    momentoActual = new Date()
    hora = momentoActual.getHours()
    minuto = momentoActual.getMinutes()
    segundo = momentoActual.getSeconds()

    horaImprimible = hora + " : " + minuto + " : " + segundo

    document.form_reloj.reloj.value = horaImprimible

    setTimeout("mueveReloj()", 1000)
}