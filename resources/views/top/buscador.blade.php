

<div class="container">

<div class="col-8">
    <div class="input-group">
        <input type="text" class="form-control" id="texto" placeholder="Ingrese nombre">
        <div class="input-group-append"><span class="input-group-text">Buscar</span></div>
    </div>
    <div id="resultados" class="bg-light border"></div>
</div>
<div class="col-8" id="contenedor">
    @if (count($topss))
    @foreach ($topss as $item)
    <p class="p-2 border-bottom">{{$item->id .' - '. $item->Name}}</p>
    @endforeach
    @endif
</div>
<div id="cargando" hidden>
    <h1>CARGANDO...</h1>
</div>
</div>



<script>
window.addEventListener('load', function() {
    document.getElementById("texto").addEventListener("keyup", () => {
        if ((document.getElementById("texto").value.length) >= 1)
            fetch(`/tops?texto=${document.getElementById("texto").value}`, {
                method: 'get'
            })
            .then(response => response.text())
            .then(html => {
                document.getElementById("resultados").innerHTML = html
            })
        else
            document.getElementById("resultados").innerHTML = ""
    })
});
</script>

