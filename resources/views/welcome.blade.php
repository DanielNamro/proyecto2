@extends('layouts.app')

@section('content')
<style>
    footer {
        position: relative;
        bottom: 0px;
    }

    #car {
        margin-bottom: 3px;
    }
</style>
<main class="py-4">
    <!--     @yield('content')-->
</main>



<div id="contenedor" class="container col-lg-10 justify-content-center">

    <form name="form_reloj" id="reloj">
        <input id="input" type="text" name="reloj" size="10">
    </form>


    <h2 id="subt">WE WILL HELP YOU TO FIND WHAT IS THE BEST FOR YOU
    </h2>


    <div class="container ">
        <div class="row ">
            <div id="carouselExampleIndicators" class="carousel slide  mx-auto d-block col-lg-10 col-md-10 col-sm-10 col-xs-12" data-ride="carousel">

                <div class="carousel-inner">
                    <div class="carousel-item active img-fluid">
                        <img height="800px" width="350" src="{{ asset('LOGO.PNG') }}" class="d-block w-100 img-fluid" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img width="300px" id="logo" src="{{ asset('Capture.PNG') }}" class="d-block w-100 img-fluid " alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="sleek-playstation-controller-picjumbo-com.jpg" class="d-block w-100 img-fluid" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev d-block" href="#carouselExampleIndicators" role="button" data-slide="prev">

                    <span class="sr-only ">Previous</span>
                </a>
                <a class="carousel-control-next d-block " href="#carouselExampleIndicators" role="button" data-slide="next">

                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <!-- Carousel wrapper -->
        <div class="row">
            <h2 id="subt2">Enjoy our best tops</h2>

            <div class="card-deck" id="ca">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" height="50px" id="car">
                    <div class="card">
                        <div class="card-body mx-auto">
                            <h5 class="card-title">Mouse</h5>
                            <p class="card-text">Today we are going to talk about the mouse, in this top we will have the limit at € 20 so below we will show you which according to our studies are the best amazon mice for less than € 20: </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/1'" class="btn btn-primary"> Go Top </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body mx-auto">
                            <h5 class="card-title">Keyboard</h5>
                            <p class="card-text">In this top we will show you the best amazon keyboards for less than € 50 to work, comfortable, soundless and precise. According to market research by our experts
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/2'" class="btn btn-primary"> Go Top </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body mx-auto">
                            <h5 class="card-title">Monitor</h5>
                            <p class="card-text">In this top we are going to talk about the best 24 "monitors on the market for less than € 200, thinking that their main use will be for work or anything else outside the gaming world.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/3'" class="btn btn-primary"> Go Top </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h4 id="subt2">Accesories</h4>

            <div class="card-deck" id="ca">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Headphones</h5>
                            <p class="card-text">In this top we will look for the best wireless helmets for less than € 20, in this case we will show only wireless helmets in case you want another top of wired helmets let us know in the comments.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/4'" class="btn btn-primary"> Go Top </button>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Microphones</h5>
                            <p class="card-text">In this top we will talk about the best microphones for less than € 200, we will look for professional microphones that offer a high quality audio perfect for working, leaving aside the microphones used for music.
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/5'" class="btn btn-primary"> Go Top </button>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Computer Mouse Pad</h5>
                            <p class="card-text">In this top we are going to present the best Amazon mats for less than € 20 for this we have not only taken into account comfort, aesthetics also the size, width, length and the material of which they are made.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/6'" class="btn btn-primary"> Go Top </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h2 id="subt2">Electronics
            </h2>


            <div class="card-deck" id="ca">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Tablets</h5>
                            <p class="card-text">In this top we will talk about the best tablets on the market below € 200 our experts have not only learned about components but have gathered different experiences with them before developing the tops so we offer great reliability on these.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/7'" class="btn btn-primary"> Go Top </button>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Mobil Phone</h5>
                            <p class="card-text">In this top we will talk about the best amazon mobiles for less than € 300, this top will continue to be updated monthly taking into account future models and including them in case they deserve to be in the top.
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/8'" class="btn btn-primary"> Go Top </button>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="car">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">GPS</h5>
                            <p class="card-text">In this top I will talk about the best GPS in the Amazon market for less than € 100, to draw our conclusions we look at opinions and experiences of both users and experts in the field who guided us to carry out this top .</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            <button onclick="location.href='/tops/9'" class="btn btn-primary"> Go Top </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

    @endsection


    </html>