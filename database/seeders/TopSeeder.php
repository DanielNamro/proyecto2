<?php

namespace Database\Seeders;

use App\Models\Top;
use Illuminate\Database\Seeder;

class TopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Top::create([
            'Name'=>'Mouse',
            'Id_Categoria'=>'1',
            'Id_User'=>'1',
            'Description'=>'Today we are going to talk about the mouse, in this top we will have the limit at € 20 so below we will show you which according to our studies are the best amazon mice for less than € 20: ',
            'pn1'=>'Logitech AOC',
            'pn2'=>'WisFox ',
            'pn3'=>'Logitech M185',
            'p1'=>'Rii RM200 Wireless Mouse with Rechargeable Battery, 5 Buttons 2.4 GHz Nano Receiver, 3 Adjustable DPI Levels Multicolor LED, Ideal for Notebooks, PCs, Computers. (Black). ',
            'p2'=>'Ratón Inalámbrico Compatible con Laptop/Macbook/iPad/Computadora Mini Ratón Silencioso Click con USB Receptor & Adaptador USB Type C para Windows/Mac/Linux/OS 3DPI Ajustable (Requiere 2*AAA Batería)',
            'p3'=>'Logitech M185 Ratón Inalámbrico, 2.4 GHz con Mini Receptor USB, Batería 12 Meses, Seguimiento Óptico 1000 DPI, Ambidiestro, PC/Mac/Portátil, Gris ',
            'enlace'=>'https://amzn.to/3bV0T8O',
            'enlace2'=>'https://amzn.to/3fqepDK',
            'enlace3'=>'https://amzn.to/3bV0T8O',
        ]);
        Top::create([
            'Name'=>'Keyboard',
            'Id_Categoria'=>'1',
            'Id_User'=>'1',
            'Description'=>'In this top we will show you the best amazon keyboards for less than € 50 to work, comfortable, soundless and precise. According to market research by our experts  ',
            'pn1'=>'Razer Cynosa Lite ',
            'pn2'=>'JOYACCESS',
            'pn3'=>'Amazon Basics',
            'p1'=>'Membrane Gaming Keyboard, Gaming, with Full Suspension Switches, Fully Programmable, RGB Chrome Views, ES Layou ',
            'p2'=>'Wireless Bluetooth Keyboard, Rechargeable Ultra-thin Full Size QWERTY Keyboard, Switchable with 3 Devices for PC, Laptops, MacBook, Compatible with Windows / Mac OS / Android, Gray ',
            'p3'=>'Wireless keyboard with Touchpad for Smart TV television - Spanish format (QWERTY), Its slim design is perfect for the living room; its 10 meter wireless range offers a good connection, even in the largest rooms ',
            'enlace'=>'https://acortar.link/h1Uu6',
            'enlace2'=>'https://amzn.to/3zaImPX',
            'enlace3'=>'https://amzn.to/3puVlHT',
        ]);
        Top::create([
            'Name'=>'Monitor',
            'Id_Categoria'=>'1',
            'Id_User'=>'1',
            'Description'=>'In this top we are going to talk about the best 24 "monitors on the market for less than € 200, thinking that their main use will be for work or anything else outside the gaming world ', 
            'pn1'=>'HUAWEI Eye Comfort AD80',
            'pn2'=>'Acer KA242Ybi',
            'pn3'=>'Lenovo L24e-30',
            'p1'=>'23.8 "FullHD Monitor (1920x1080 Pixels, Full HD, FullView 1080P Screen, IPS Anti-Glare, Frames Only 5.7mm, Screen-to-Body Ratio 90%), Color Black ',
            'p2'=>'23.8 "Full HD 75 Hz Monitor (60.5 cm, 1920x1080, IPS LED Screen, ZeroFrame and FreeSync, 250 nits, 1ms Response Time, VGA Connector) - Color Black ',
            'p3'=>'23.8 "Monitor (Full HD Screen, 1920x1080 Pixels, 75Hz, 4 ms, HDMI + VGA Ports, HDMI Cable) Adjustable tilt, Plastic base, Color Black ',
            'enlace'=>'https://amzn.to/3wX628y',
            'enlace2'=>'https://amzn.to/3v0adij',
            'enlace3'=>'https://amzn.to/3v2du0L',
        ]);

        Top::create([
            'Name'=>'Headphones',
            'Id_Categoria'=>'2',
            'Id_User'=>'1',
            'Description'=>'In this top we will look for the best wireless helmets for less than € 20, in this case we will show only wireless helmets in case you want another top of wired helmets let us know in the comments ',
            'pn1'=>'HOMSCAM',
            'pn2'=>'6S',
            'pn3'=>'S OIFIO',
            'p1'=>'Bluetooth Headphones, HOMSCAM Wireless Headphones QCY Bluetooth 5.0 Stereo Sound Earphone Mini Twins In-Ear Headphones Quick Charge Waterproof with Charging Box ',
            'p2'=>'6S Wireless Closed Headphones, Hi-Fi Stereo Foldable Wireless Stereo Headphones, Built-in Microphone, Micro SD / TF, FM (for iPhone / Samsung / iPad / PC) (White and Rose Gold) ',
            'p3'=>'Bluetooth Headphones, Wireless Headphones, In-ear Headphones, Noise Canceling Headphones, Suitable for Bluetooth DeviceBrand: S OIFIO ',
            'enlace'=>'https://amzn.to/3irUtSS',
            'enlace2'=>'https://amzn.to/3gi6QOv',
            'enlace3'=>'https://amzn.to/3527WJ7',
        ]);

        Top::create([
            'Name'=>'Microphones',
            'Id_Categoria'=>'2',
            'Id_User'=>'1',
            'Description'=>'In this top we will talk about the best microphones for less than € 200, we will look for professional microphones that offer a high quality audio perfect for working, leaving aside the microphones used for music. ',
            'pn1'=>'Yeti',
            'pn2'=>'TONOR',
            'pn3'=>'Razer Seiren X',
            'p1'=>'Blue Yeti Professional USB Microphone for Recording, Streaming, Podcasting, Broadcasting, Gaming, Voiceover and More, Multi-Pattern, Plugn Play on PC and Mac - Black',
            'p2'=>'TONOR Micro USB Condenser Microphone Recording Cardioid Polar Pattern for Recording Music and Video Podcast Live Streaming Games Chat Arm Support ',
            'p3'=>'Razer Seiren X streaming USB condenser microphone, compact with damper, supercardioid recording pattern, no latency, mute button, headphone jack, black ',
            'enlace'=>'https://amzn.to/35cBxjf',
            'enlace2'=>'https://amzn.to/3xjgtUf',
            'enlace3'=>'https://amzn.to/3il6edI',
        ]);



        Top::create([
            'Name'=>'Computer Mouse Pad',
            'Id_Categoria'=>'2',
            'Id_User'=>'1',
            'Description'=>'In this top we are going to present the best Amazon mats for less than € 20 for this we have not only taken into account comfort, aesthetics also the size, width, length and the material of which they are made.             ',
            'pn1'=>'Havit',
            'pn2'=>'Anpollo',
            'pn3'=>'Beexcellent',
            'p1'=>'havit Extended XXXL Computer Mouse Pad (1000 x 430 x 4 mm), Waterproof Surface and Non-Slip Rubber Base for Gamers, PC and Laptop (MP857) ',
            'p2'=>'Anpollo Gaming Mouse Pad 900 x 400 World Map Mouse Pad, Non-slip Rubber Base, Suitable for Gamers, PC and Laptop  ',
            'p3'=>'Beexcellent Gaming Mat, 800x300x5mm 14 Adjustable RGB Lighting Modes and 4 USB Ports. Super Thick Gaming Mouse Pad, Waterproof and Non-slip Function ',
            'enlace'=>'https://amzn.to/3gcdZBj',
            'enlace2'=>'https://amzn.to/3w7AMDI',
            'enlace3'=>'https://amzn.to/2TbpZtD',
        ]);


        Top::create([
            'Name'=>'Tablets',
            'Id_Categoria'=>'3',
            'Id_User'=>'1',
            'Description'=>'In this top we will talk about the best tablets on the market below € 200 our experts have not only learned about components but have gathered different experiences with them before developing the tops so we offer great reliability on these   ',
            'pn1'=>'Blackview Tab8',
            'pn2'=>'Anpollo',
            'pn3'=>'Beexcellent',
            'p1'=>'Blackview Tab8 Tablet 10.1 Pulgadas Android 10 4G LTE 5G WIFI, 4GB RAM+64GB ROM (TF 128GB), Octa-Core, Batería 6580mAh, Tableta con Cámara 13MP+5MP, 1920*1200, Dual SIM/Face ID/GPS/OTG/Bluetooth-Gris ',
            'p2'=>'Anpollo Gaming Mouse Pad 900 x 400 World Map Mouse Pad, Non-slip Rubber Base, Suitable for Gamers, PC and Laptop  ',
            'p3'=>'Beexcellent Gaming Mat, 800x300x5mm 14 Adjustable RGB Lighting Modes and 4 USB Ports. Super Thick Gaming Mouse Pad, Waterproof and Non-slip Function ',
            'enlace'=>'https://amzn.to/3gcdZBj',
            'enlace2'=>'https://amzn.to/3w7AMDI',
            'enlace3'=>'https://amzn.to/2TbpZtD',
        ]);

        Top::create([
            'Name'=>'Mobil Phone',
            'Id_Categoria'=>'3',
            'Id_User'=>'6',
            'Description'=>'In this top we will talk about the best amazon mobiles for less than € 300, this top will continue to be updated monthly taking into account future models and including them in case they deserve to be in the top.  ',
            'pn1'=>'Xiaomi Redmi 9',
            'pn2'=>'POCO X3 Pro',
            'pn3'=>'Samsung Galaxy A21s',
            'p1'=>'Smartphone with 6.53 "DotDisplay FHD + Screen, 4 GB and 64 GB, 13 MP Quad Camera with AI, MediaTek Helio G80, 5020 mAh Battery, 18 W Fast Charge, Green ',
            'p2'=>'Smartphone 6 + 128 GB, 6.67 ”120Hz FHD + DotDisplay, Snapdragon 860, Quadruple Camera 48 MP, 5160 mAh, Phantom Black ',
            'p3'=>'Samsung Galaxy A21s - Smartphone de 6.5" (4 GB RAM, 128 GB de Memoria Interna, WiFi, Procesador Octa Core, Cámara Principal de 48 MP, Android 10.0) Color Azul  ',
            'enlace'=>'https://amzn.to/3wygdRj',
            'enlace2'=>'https://amzn.to/3vnUauW',
            'enlace3'=>'https://amzn.to/3iEmRBo',
        ]);


        Top::create([
            'Name'=>'Mobil Phone',
            'Id_Categoria'=>'3',
            'Id_User'=>'6',
            'Description'=>'In this top we will talk about the best amazon mobiles for less than € 300, this top will continue to be updated monthly taking into account future models and including them in case they deserve to be in the top.  ',
            'pn1'=>'Xiaomi Redmi 9',
            'pn2'=>'POCO X3 Pro',
            'pn3'=>'Samsung Galaxy A21s',
            'p1'=>'Smartphone with 6.53 "DotDisplay FHD + Screen, 4 GB and 64 GB, 13 MP Quad Camera with AI, MediaTek Helio G80, 5020 mAh Battery, 18 W Fast Charge, Green ',
            'p2'=>'Smartphone 6 + 128 GB, 6.67 ”120Hz FHD + DotDisplay, Snapdragon 860, Quadruple Camera 48 MP, 5160 mAh, Phantom Black ',
            'p3'=>'Samsung Galaxy A21s - Smartphone de 6.5" (4 GB RAM, 128 GB de Memoria Interna, WiFi, Procesador Octa Core, Cámara Principal de 48 MP, Android 10.0) Color Azul  ',
            'enlace'=>'https://amzn.to/3wygdRj',
            'enlace2'=>'https://amzn.to/3vnUauW',
            'enlace3'=>'https://amzn.to/3iEmRBo',
        ]);
        Top::create([
            'Name'=>'Mobil Phone',
            'Id_Categoria'=>'3',
            'Id_User'=>'6',
            'Description'=>'In this top I will talk about the best GPS in the Amazon market for less than € 100, to draw our conclusions we look at opinions and experiences of both users and experts in the field who guided us to carry out this top ',
            'pn1'=>'TomTom',
            'pn2'=>'Garmin Drive 5',
            'pn3'=>'LAYDRAN',
            'p1'=>'TomTom Car GPS Start 52 Lite, 5 Inch, EU Maps, Integrated Reversible Mount [Amazon Exclusive] ',
            'p2'=>'Livetrack function that allows the driver to share the location so that his friends know where he is. Driving alerts that inform you of sharp curves, speed changes, speed cameras and more ',
            'p3'=>'LAYDRAN Sistema de navegación GPS para camión, portátil, satélite, de 7 pulgadas, con cámara de velocidad y pantalla táctil de guía de voz y mapa de navegador de por vida de la UE 46 países 2020  ',
            'enlace'=>'https://amzn.to/2RSiA21',
            'enlace2'=>'https://amzn.to/3vnUauW',
            'enlace3'=>'https://amzn.to/3xjQE6q',
        ]);
    }
}
